const ldap = require("ldapjs");
const assert = require("assert");

/**
 * Search a directory using a person's name
 * @param {*} name The full name of the person
 * @param {*} source The ldap directory url (without 'ldap://')
 * @param {*} callback will contain the gathered entries
 */
const authenticateDN = (name, source, callback) => {
  const client = ldap.createClient({
    url: `ldap://${source}`,
  });

  // options for the query
  const opts = {
    filter: `(&(ou=Medical School - Faculty and Staff)(cn=${name}))`,
    scope: "sub",
    attributes: ["cn", "ou", "mail"], // name, organisation, mail address
  };

  // make the connection to the server
  client.bind("", "", (err) => {
    if (err) {
      console.log("Error in new connection:", err);
    } else {
      console.log("Success");

      // search the directory using the specified options
      // TODO make the dc options configurable
      client.search("dc=umich,dc=edu", opts, (err, res) => {
        assert.ifError(err);

        const entries = [];

        // handle the different kind of statuses
        res.on("searchEntry", (entry) => {
          entries.push(entry.object);
        });
        res.on("searchReference", (referral) => {
          console.log("referral:", referral.uris.join());
        });
        res.on("error", (err) => {
          console.error("error:", err.message);
        });
        res.on("end", (result) => {
          console.log("status:", result.status);
          callback(entries);
        });
      });
    }
  });
};

module.exports = authenticateDN;
