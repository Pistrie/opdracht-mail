require("dotenv").config();
const express = require("express");
const app = express();

const ldapSearch = require("./ldap-search");
const exportContactsToFile = require("./export-contacts-to-file");

app.listen(3000, () => {
  console.log("server started");
});

// cn is first + last name
const name = process.env.NAME;
const source = process.env.SOURCE || "ldap.itd.umich.edu";

// authenticateDN(name, source);
ldapSearch(name, source, (result) => {
  exportContactsToFile(result, "contacts.json");
});
