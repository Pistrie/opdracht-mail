const fs = require("fs");

/**
 * Exports the given contacts to a file
 * @param {json} contacts a json object containing at least 'cn' and 'mail' from the ldap query
 * @param {string} fileLocation the location of the file containing the exported contacts
 */
const exportToFile = (contacts, fileLocation) => {
  // loop through contacts object and write important bits to the file
  fs.writeFile(fileLocation, "[", (err) => {
    if (err) {
      console.error("Error:", err);
    }
    contacts.forEach((contact) => {
      const cleanedContact = {
        commonName: contact.cn[0],
        mail: contact.mail,
      };
      fs.appendFile(
        fileLocation,
        JSON.stringify(cleanedContact) + ",",
        (err) => {
          if (err) {
            console.error("Error:", err);
          }
        }
      );
    });
    fs.appendFile(fileLocation, "]", (err) => {
      if (err) {
        console.error("Error:", err);
      }
    });
  });
};

module.exports = exportToFile;
